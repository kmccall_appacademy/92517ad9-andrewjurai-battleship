class Board
  attr_reader :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    ten_by_one = Array.new
    10.times { ten_by_one << nil }

    ten_by_ten = Array.new
    10.times { ten_by_ten << ten_by_one }

    ten_by_ten
  end

  def count
    @grid.flatten.count(:s)
  end

  def [](coord_arr)
    x = coord_arr.first
    y = coord_arr.last
    @grid[x][y]
  end

  def empty?(pos_arr = nil)
    if pos_arr.nil?
      return count == 0 ? true : false
    end
    x = pos_arr.first
    y = pos_arr.last
    @grid[x][y].nil?
  end

  def full?
    count == @grid.flatten.count ? true : false
  end

  def place_random_ship
    column = row = @grid.length
    self.full? ? (raise "grid full") : (@grid[rand(row)][rand(column)] = :s)
  end

  def won?
    count == 0
  end


end

require 'byebug'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player #player_instance
    @board = board #board instance
  end

  def attack(attack_coord_arr)
    x = attack_coord_arr.first
    y = attack_coord_arr.last
    @board.grid[x][y] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    attack_coord_arr = @player.get_play
    attack(attack_coord_arr)
  end


end
